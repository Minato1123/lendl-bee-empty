class Bomb extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y) {
    super(scene, x, y, "bomb");
    this.scene = scene;
    scene.physics.world.enable(this);
    scene.add.existing(this);
  }
  
  update(){
    this.setVelocityY(100);
    if(this.y>600){
      this.disableBody(true, true);
    }
  }
}