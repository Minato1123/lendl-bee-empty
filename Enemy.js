class Enemy extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y, left, right) {
    super(scene, x, y, "enemy");
    this.scene = scene;
    scene.physics.world.enable(this);
    scene.add.existing(this);
    this.setCollideWorldBounds(true);
    this.direction = -1;
    this.lastDownTime=new Date().getTime();
    this.left=left;
    this.right=right;
  }

  update() {
    if (this.direction == -1) {
      if (this.x <= this.left) {
        this.direction = 1;
      }
    } else {
      if (this.x >= this.right) {
        this.direction = -1;
      }
    }
    this.setVelocityX(this.direction*50);
    let now=new Date().getTime();
    if((now-this.lastDownTime)>5000){
      this.y+=10;
      this.lastDownTime=now;
      if(Math.random()>0.8 && this.body.enable){
        let bomb=new Bomb(this.scene, this.x, this.y);
        this.scene.bombs.add(bomb);
        bomb.update();
      }
    }
  }
}
