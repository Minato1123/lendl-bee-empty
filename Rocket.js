class Rocket extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y) {
    super(scene, x, y, "rocket");
    this.scene = scene;
    scene.physics.world.enable(this);
    scene.add.existing(this);
    this.setCollideWorldBounds(true);
    this.left = false;
    this.right = false;
  }

  goLeft() {
    this.left = true;
  }
  goRight() {
    this.right = true;
  }
  idle() {
    this.left = this.right = false;
  }
  
  
  update(){
    if (this.left) {
      this.setVelocityX(-160);
    } else if (this.right) {
      this.setVelocityX(160);
    } else {
      this.setVelocity(0, 0);
    }
  }
}