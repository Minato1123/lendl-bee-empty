/*
 * 定義一個 scene，用成員變數儲存 scene 上面的物件
 * override preload, create, update 函式
 */
class Scene extends Phaser.Scene {
  /*********************************************************************************************************************** */
  constructor() {
    super();
    this.lastFireTime = null;
  }

  /*********************************************************************************************************************** */
  preload() {
    this.load.image(
      "rocket",
      "https://cdn.glitch.com/6722a49c-8d78-497e-a2cd-4cddf933e082%2FSpaceship_01_ORANGE_20.png?v=1583471811462"
    );

    this.load.image(
      "enemy",
      "https://cdn.glitch.com/6722a49c-8d78-497e-a2cd-4cddf933e082%2Frocket%2011.png?v=1583472117475"
    );

    this.load.image(
      "bullet",
      "https://cdn.glitch.com/6722a49c-8d78-497e-a2cd-4cddf933e082%2FFlame_01_small.png?v=1583473752493"
    );
    this.load.image(
      "block",
      "https://cdn.glitch.com/6722a49c-8d78-497e-a2cd-4cddf933e082%2F6722a49c-8d78-497e-a2cd-4cddf933e082_Gray_10.png?v=1583722580899"
    );
    this.load.image(
      "bomb",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fbomb.png?v=1583115128502"
    );

  }

  /*********************************************************************************************************************** */
  create() {
    this.rocket = new Rocket(this, 400, 570);
    this.cursors = this.input.keyboard.createCursorKeys();
    this.enemies = this.physics.add.group();
    this.bullets = this.physics.add.group();
    this.bombs = this.physics.add.group();
    this.blocks = this.physics.add.group();
    for (let x=100; x<=700; x+=50) {
      for (let y=40; y<=200; y+=40) {
        let enemy = new Enemy(this, x, y, x-70, x+70);
        this.enemies.add(enemy);
      }
    }

    for (let y=450; y<=530; y+=10) {
      for (let x=50; x<=750; x+=10) {
        if (Math.random() > 0.6) {
          let block = new Block(this, x, y);
          this.blocks.add(block);
        }
      }
    }

    this.physics.add.overlap(this.bullets, this.enemies, this.disableEachOther);
    this.physics.add.overlap(this.bullets, this.bombs, this.disableEachOther);
    this.physics.add.overlap(this.rocket, this.bombs, this.disableEachOther);
    this.physics.add.overlap(this.blocks, this.bullets, this.disableEachOther);
    this.physics.add.overlap(this.blocks, this.bombs, this.disableEachOther);
    this.physics.add.overlap(this.blocks, this.enemies, (o1, o2) => {
      o1.disableBody(true, true);
    });



    
  }

  disableEachOther(o1, o2) {
    o1.disableBody(true, true);
    o2.disableBody(true, true);
  }
  /*********************************************************************************************************************** */
  update() {
    this.rocket.idle();
    if (this.cursors.space.isDown && this.rocket.body.enable) {
      let now = new Date().getTime();
      if (!this.lastFireTime || now-this.lastFireTime > 300) {
        let bullet = new Bullet(this, this.rocket.x, this.rocket.y-20);
        this.bullets.add(bullet);
        bullet.update();
        this.lastFireTime = now;
      }
    }

    if (this.cursors.left.isDown) {
      this.rocket.goLeft();
    } else if (this.cursors.right.isDown) {
      this.rocket.goRight();
    }
    this.rocket.update();
    this.enemies.children.iterate((e)=>{
      if (e.y > 590) {
        this.rocket.disableBody(true, true)
      }
      e.update();
    })
  }
    
  
}
